# BasicTransitionCSharp

<div align="center">
<img src="csharp.png"/>
</div>


# Properties


Controls

Speed

Ease

Type

Shift

IsReverseShift

IsFading


# Methods


.Container(<List of controls>)

.Run(<control>)


# Code


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Kitchanismo;

namespace BasicSampleTransition
{
    public partial class SampleUserControls : Form
    {
        public SampleUserControls()
        {
            InitializeComponent();

            _transition = new KitchanismoTransition();

            _gold = new GoldView();
            _green = new GreenView();
            _indigo = new IndigoView();

            InitializeTransition();
        }

        KitchanismoTransition _transition;

        GoldView _gold;
        GreenView _green;
        IndigoView _indigo;

        private void InitializeTransition()
        {
            try
            {
                
                //Properties
                _transition.Speed          = 800;
                _transition.Ease           = Easing.QuartInOut;
                _transition.Type           = Types.Intersect;
                _transition.Shift          = Shift.XY;
                _transition.IsReverseShift = true;
                _transition.IsFading       = false;

                //List should be 2 or more controls)
                //First array should be the active
                var userControls = new List<UserControl>()
                {
                   _gold,
                   _green,
                   _indigo
                };

                _transition.Controls.AddRange(userControls);

                _transition.Container(PanelContainer);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void BtnGreen_Click(object sender, EventArgs e)
        {
            _transition.Run(_green);
        }

        private void BtnIndigo_Click(object sender, EventArgs e)
        {
            _transition.Run(_indigo);
        }

        private void BtnGold_Click(object sender, EventArgs e)
        {
            _transition.Run(_gold);
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

