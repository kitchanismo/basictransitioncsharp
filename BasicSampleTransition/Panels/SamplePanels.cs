﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//import namespace
using Kitchanismo;

namespace BasicSampleTransition
{
    public partial class SamplePanels : Form
    {
       
        public SamplePanels()
        {
            InitializeComponent();

            _transition = new KitchanismoTransition();

           InitializeTransition();
        }

        private KitchanismoTransition _transition;

        private void InitializeTransition()
        {
            try
            {

                //Properties 

                _transition.Speed          = 800;
                _transition.Ease           = Easing.CubicInOut;
                _transition.Type           = Types.Swap;
                _transition.Shift          = Shift.Y;
                _transition.IsReverseShift = true;
                _transition.IsFading       = true;

                //List should be 2 or more controls)
                //First array should be the active
                var panels = new List<Panel>()
                {
                   PanelGold,
                   PanelGreen,
                   PanelIndigo
                };

                //Add Panels
                _transition.Controls.AddRange(panels);

                //Set Container
                _transition.Container(PanelContainer);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
}

        private void BtnGreen_Click(object sender, EventArgs e)
        {
            _transition.Run(PanelGreen);
        }
        
        private void BtnIndigo_Click(object sender, EventArgs e)
        {
            _transition.Run(PanelIndigo);
        }
        
        private void BtnGold_Click(object sender, EventArgs e)
        {
            _transition.Run(PanelGold);
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
