﻿namespace BasicSampleTransition
{
    partial class SamplePanels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelContainer = new System.Windows.Forms.Panel();
            this.PanelGold = new System.Windows.Forms.Panel();
            this.PanelIndigo = new System.Windows.Forms.Panel();
            this.PanelGreen = new System.Windows.Forms.Panel();
            this.BtnGreen = new System.Windows.Forms.Button();
            this.BtnIndigo = new System.Windows.Forms.Button();
            this.BtnGold = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PanelContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelContainer
            // 
            this.PanelContainer.Controls.Add(this.PanelGold);
            this.PanelContainer.Controls.Add(this.PanelIndigo);
            this.PanelContainer.Controls.Add(this.PanelGreen);
            this.PanelContainer.Location = new System.Drawing.Point(16, 81);
            this.PanelContainer.Name = "PanelContainer";
            this.PanelContainer.Size = new System.Drawing.Size(767, 473);
            this.PanelContainer.TabIndex = 0;
            // 
            // PanelGold
            // 
            this.PanelGold.BackColor = System.Drawing.Color.Gold;
            this.PanelGold.Location = new System.Drawing.Point(29, 128);
            this.PanelGold.Name = "PanelGold";
            this.PanelGold.Size = new System.Drawing.Size(767, 473);
            this.PanelGold.TabIndex = 3;
            // 
            // PanelIndigo
            // 
            this.PanelIndigo.BackColor = System.Drawing.Color.Indigo;
            this.PanelIndigo.Location = new System.Drawing.Point(73, 75);
            this.PanelIndigo.Name = "PanelIndigo";
            this.PanelIndigo.Size = new System.Drawing.Size(767, 473);
            this.PanelIndigo.TabIndex = 2;
            // 
            // PanelGreen
            // 
            this.PanelGreen.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.PanelGreen.Location = new System.Drawing.Point(110, 25);
            this.PanelGreen.Name = "PanelGreen";
            this.PanelGreen.Size = new System.Drawing.Size(767, 473);
            this.PanelGreen.TabIndex = 1;
            // 
            // BtnGreen
            // 
            this.BtnGreen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnGreen.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGreen.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.BtnGreen.Location = new System.Drawing.Point(447, 17);
            this.BtnGreen.Name = "BtnGreen";
            this.BtnGreen.Size = new System.Drawing.Size(108, 54);
            this.BtnGreen.TabIndex = 1;
            this.BtnGreen.Text = "Green";
            this.BtnGreen.UseVisualStyleBackColor = true;
            this.BtnGreen.Click += new System.EventHandler(this.BtnGreen_Click);
            // 
            // BtnIndigo
            // 
            this.BtnIndigo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnIndigo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnIndigo.ForeColor = System.Drawing.Color.Indigo;
            this.BtnIndigo.Location = new System.Drawing.Point(561, 17);
            this.BtnIndigo.Name = "BtnIndigo";
            this.BtnIndigo.Size = new System.Drawing.Size(108, 54);
            this.BtnIndigo.TabIndex = 2;
            this.BtnIndigo.Text = "Indigo";
            this.BtnIndigo.UseVisualStyleBackColor = true;
            this.BtnIndigo.Click += new System.EventHandler(this.BtnIndigo_Click);
            // 
            // BtnGold
            // 
            this.BtnGold.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnGold.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnGold.ForeColor = System.Drawing.Color.Gold;
            this.BtnGold.Location = new System.Drawing.Point(675, 17);
            this.BtnGold.Name = "BtnGold";
            this.BtnGold.Size = new System.Drawing.Size(108, 54);
            this.BtnGold.TabIndex = 3;
            this.BtnGold.Text = "Gold";
            this.BtnGold.UseVisualStyleBackColor = true;
            this.BtnGold.Click += new System.EventHandler(this.BtnGold_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 36);
            this.label1.TabIndex = 4;
            this.label1.Text = "Panels";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Location = new System.Drawing.Point(751, 557);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "close";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // SamplePanels
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 576);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnGold);
            this.Controls.Add(this.BtnIndigo);
            this.Controls.Add(this.BtnGreen);
            this.Controls.Add(this.PanelContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SamplePanels";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BasicUsage";
            this.PanelContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PanelContainer;
        private System.Windows.Forms.Panel PanelGold;
        private System.Windows.Forms.Panel PanelIndigo;
        private System.Windows.Forms.Panel PanelGreen;
        private System.Windows.Forms.Button BtnGreen;
        private System.Windows.Forms.Button BtnIndigo;
        private System.Windows.Forms.Button BtnGold;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}